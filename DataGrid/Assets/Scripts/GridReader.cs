using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridReader : MonoBehaviour
{
    DataGrid grid;
    // Start is called before the first frame update
    void Start()
    {
       grid  = Resources.Load("Grids/test") as DataGrid;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            for (int i = 0; i < grid.ElementGrid.Length; i++)
            {
                for (int j = 0; j < grid.ElementGrid[i].Column.Length; j++)
                {
                    Debug.Log(grid.ElementGrid[i].Column[j].Name);
                }
            }
        }
        
    }
}
