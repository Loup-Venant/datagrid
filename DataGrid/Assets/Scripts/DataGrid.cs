using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
 public class ElementDataRow
 {
     public ElementData[] Column;
     
     public ElementDataRow(int column)
     {
         this.Column = new ElementData[column];
     }
 }
 [System.Serializable]
public class DataGrid : ScriptableObject
{
    public string Name;
    public ElementDataRow[] ElementGrid;
}
