using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class GridCreator : EditorWindow
{
    static GridCreator window;
    int Row;
    int Column;

    string gridName = "New Untilted Grid";
    [MenuItem("Grid Creator/Open Grid Creator Window")]
    static void ShowWindow()
    {
        window = GetWindow<GridCreator>();
            
        window.titleContent = new GUIContent("Grid Creator");
        window.Show();
    }
    void OnGUI()
    {
        DrawMenuBar();
        
        if(GUI.changed)
        {
            Repaint();
        }
        
    }

    void DrawMenuBar()
    {
        Rect menuBar = new Rect(0, 0, position.width, position.height);
        GUILayout.BeginArea(menuBar);
        GUILayout.BeginVertical();
        gridName = GUILayout.TextField(gridName, GUILayout.Width(144), GUILayout.Height(20));
        if(GUILayout.Button("Save Grid", GUILayout.Width(144), GUILayout.Height(42)))
        {
            string path = AssetDatabase.GenerateUniqueAssetPath("Assets/Resources/Grids/" + gridName + ".asset");
            DataGrid grid = DataGrid.CreateInstance<DataGrid>();
            SetupGridData(grid);
            AssetDatabase.CreateAsset(grid, path);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = grid;
        }
        GUILayout.EndVertical();
        GUILayout.EndArea();
    }

    void SetupGridData(DataGrid grid)
    {
        grid.Name = gridName;
        grid.ElementGrid = new ElementDataRow[Row];
        foreach( ElementDataRow row in grid.ElementGrid)
        {
            row.Column = new ElementData[Column];
        }
        for (int i = 0; i < grid.ElementGrid.Length; i++)
        {
            for (int j = 0; j < grid.ElementGrid[i].Column.Length; j++)
            {
                ElementData element = new ElementData();
                element.Name = "Name " + i + j;
                grid.ElementGrid[i].Column[j] = element;
                
            }
        }
    }

}
